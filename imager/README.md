# Imager

Quickly get a Linux image of your choice you can use directly in the command line.

The script outputs absolute path to an image you select and downloads it from a mirror if necessary.

```sh
qemu-system-x86_64 `imager --pwd=SuperSecret f40`
```

If the output of the script is not a terminal, output of all commands is redirected to a `/tmp/imager.log.XXXXXXXXX`.

## Usage

```sh
imager [OPTIONS] IMAGE
```

Where `IMAGE` is IMAGE name.
- `fXX` for Fedora XX Generic Cloud image
- `rawhide` for Fedora Rawhide Generic Cloud image

### Options

- `--arch=ARCH` - image architecture (default: `x86_64`)
- `--update` - fetch newest image from mirror even if it is already downloaded
- `--cache=CACHE_DIR` - directory to save images to (default: `$HOME/.cache/imager`)
- `--no-sysprep` - do not change root password after the image is downloaded
- `--pwd=PASSWORD` - root password (default: `password`)
