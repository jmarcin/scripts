#!/bin/bash

set -e

TO=$1
FROM=$2

if [ -z "$TO" ]; then
    exit 2
fi

if [ -n "$FROM" ]; then
    if [ "$FROM" = "version" ]; then
        FROM="--from-version"
    else
        FROM="--from=$FROM"
    fi
fi

pkgpatch --src-git ./repo --dist-git ./dist/checkpolicy     $FROM --to="$TO" --subdir=checkpolicy
pkgpatch --src-git ./repo --dist-git ./dist/libselinux      $FROM --to="$TO" --subdir=libselinux
pkgpatch --src-git ./repo --dist-git ./dist/libsemanage     $FROM --to="$TO" --subdir=libsemanage
pkgpatch --src-git ./repo --dist-git ./dist/libsepol        $FROM --to="$TO" --subdir=libsepol
pkgpatch --src-git ./repo --dist-git ./dist/mcstrans        $FROM --to="$TO" --subdir=mcstrans
pkgpatch --src-git ./repo --dist-git ./dist/policycoreutils $FROM --to="$TO"
pkgpatch --src-git ./repo --dist-git ./dist/secilc          $FROM --to="$TO" --subdir=secilc
pkgpatch --src-git ./repo --dist-git ./dist/checkpolicy     $FROM --to="$TO" --subdir=checkpolicy
