#!/bin/bash
#
# Author: Juraj Marcin <jmarcin@redhat.com>
#
# Reads `/tmp/BACKPORTED_BRANCHES` file as list of branches, switches to each of
# them and pushes is to $FORK remote repository

set -e

FORK=${FORK:-redhat-fork}

BRANCHES=`cat /tmp/BACKPORTED_BRANCHES`

for BRANCH in $BRANCHES; do
    git switch "$BRANCH"
    git push $FORK "$@"
done

rm -f "/tmp/BACKPORTED_BRANCHES"
