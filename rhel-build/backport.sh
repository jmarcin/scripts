#!/bin/bash
#
# Author: Juraj Marcin <jmarcin@redhat.com>
#
# Usage:
#
# ./backport.sh <<EOF
# ISSUE_ID COMMIT [COMMIT...]
#
# Will cherry-pick each COMMIT in the branch ISSUE_ID created from the current
# HEAD and checks/fixes `Resolves: ISSUE_ID` in the commit description.
# After that it will create `/tmp/BACKPORTED_BRANCHES` file with names of
# backported branches.
# 
# One then can push individual branches with `push-fork.sh` script and then for
# each:
#   1. Create MR
#   2. Merge MR
#   3. Continue
#
# This way one can have MR for each fixed issue in the buid without the need to
# rebase/create merge commits

set -e

BRANCHES=""
LAST_GOOD="`git branch | grep '*' | awk '{ print $2 }'`"

function resolve_conflicts {
    while true; do
        echo "Please resolve conflict, starting shell..."
        if SHELL_STATUS='(resolve) ' $SHELL -i; then
            git cherry-pick --continue
            return
        else
            git cherry-pick --abort
            git switch "$LAST_GOOD"
            git branch -D $BRANCHES
            return 1
        fi
    done
}

while read -r ISSUE COMMITS; do

    git switch -c "$ISSUE"
    BRANCHES="$BRANCHES $ISSUE"

    for COMMIT in $COMMITS; do

        if [[ "$COMMIT" =~ ^http.* ]]; then
            if ! curl "$COMMIT" | git am; then
                resolve_conflicts
            fi
        else
            if ! git cherry-pick "$COMMIT"; then
                resolve_conflicts
            fi
        fi

        COMMIT_MSG=`git show -s --format="%B" "$COMMIT" | sed -e '/./,$!d' -e :a -e '/^\n*$/{$d;N;ba' -e '}'`
        if ! echo "$COMMIT_MSG" | grep -q "^Resolves: $ISSUE\$"; then
            if echo "$COMMIT_MSG" | grep "^Resolves:"; then
                if [ `echo "$COMMIT_MSG" | grep -c "^Resolves:"` -eq 1 ]; then
                    COMMIT_MSG=`echo "$COMMIT_MSG" | sed "s/^Resolves:.*$/Resolves: $ISSUE/"`
                else
                    git commit --ammend
                    continue
                fi
            else
                COMMIT_MSG=`echo -en "$COMMIT_MSG\n\nResolves: $ISSUE"`
            fi
            echo "$COMMIT_MSG" | git commit --amend --file -
        fi

    done

done

echo -e "Successfully backported: $BRANCHES"
echo "$BRANCHES" > /tmp/BACKPORTED_BRANCHES
