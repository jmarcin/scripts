from itertools import cycle
from random import shuffle
from subprocess import PIPE, Popen
from threading import Thread
from typing import Iterator
from sys import argv


def colors() -> Iterator[int]:
    tmp = [1, 2, 3, 4, 5, 6, 10, 11, 12, 13, 14]
    shuffle(tmp)
    return cycle(tmp)


COLORS = colors()


Pkgs = dict[str, "Pkgs"]

PACKAGES: Pkgs = {
    "libsepol": {
        "libselinux": {
            "checkpolicy": {},
            "mcstrans": {},
            "secilc": {},
            "libsemanage": {"policycoreutils": {}},
        },
    },
}


def printc(*args, color: int, end: str = "\n", **kwargs) -> None:
    print(f"\033[38;5;{color}m", end="")
    print(*args, **kwargs, end="")
    print("\033[0m", end=end)


def prefix_run(args: list[str], color: int, prefix: str) -> int:
    with Popen(args, bufsize=1, text=True, stdout=PIPE) as process:
        while process.poll() is None:
            while process.stdout is None:
                pass
            line = process.stdout.readline()
            printc(f"{prefix}:", color=color, end=" ")
            print(line, end="")
        return process.returncode


def builder(pkg_name: str, sub_pkgs: Pkgs) -> None:
    color = next(COLORS)
    printc(f"=====  Building {pkg_name}...  =====", color=color)
    rc = prefix_run(
        [
            "copr-cli",
            "build",
            "jmarcin/selinux-testing",
            f"srpms/{pkg_name}.src.rpm"
        ],
        color,
        pkg_name,
    )
    if rc == 0:
        printc(f"=====  {pkg_name} built!  =====", color=color)
        build(sub_pkgs)
    else:
        printc(f"=====  {pkg_name} failed with {rc}!  =====", color=9)


def build(pkgs: Pkgs) -> None:
    threads: list[Thread] = []
    for pkg_name, sub_pkgs in pkgs.items():
        threads.append(Thread(target=builder, args=(pkg_name, sub_pkgs)))
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()


if __name__ == "__main__":
    build(PACKAGES)
