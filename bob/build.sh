#!/bin/bash

set -e
set -o pipefail

#
# Utility functions
#

function warn {
    echo "$@" >&2
}

function error {
    CODE="$1"
    shift
    warn "$@"
    exit "$CODE"
}

function run {
    if [ -n "$BOB_DRY_RUN" ]; then
        echo "= $@"
    else
        echo "+ $@"
    fi
    if [ -z "$BOB_DRY_RUN" ]; then
        "$@"
    fi
}

function peek_line {
    head -n1 "$1"
}

function shift_line {
    peek_line "$1"
    sed -i '1d' "$1"
}

function peek_last_line {
    tail -n1 "$1"
}

function shift_last_line {
    peek_last_line "$1"
    sed -i '$d' "$1"
}

#
# Git utility functions
#

function src_git_q {
    git -C "$BOB_SRC_GIT" "$@"
}

function src_git {
    run git -C "$BOB_SRC_GIT" "$@"
}

function dist_git_q {
    git -C "$BOB_DIST_GIT" "$@"
}

function dist_git {
    run git -C "$BOB_DIST_GIT" "$@"
}

function switch_main {
    dist_git switch "$BOB_DIST_GIT_MAIN"
}

function sync_main {
    switch_main
    dist_git pull "$BOB_DIST_GIT_UPSTREAM"
}

#
# Settings
#

# Files

BOB_DIR="${BOB_DIR:-.bob_build}"
BOB_TODO_FILE="${BOB_TODO_FILE:-${BOB_DIR}/todo}"
BOB_JOURNAL_FILE="${BOB_JOURNAL_FILE:-${BOB_DIR}/journal}"
BOB_COMMITS_FILE="${BOB_COMMITS_FILE:-${BOB_DIR}/commits}"
BOB_CHANGELOG_FILE="${BOB_CHANGELOG_FILE:-${BOB_DIR}/changelog}"
BOB_SETTINGS_FILE="${BOB_SETTINGS_FILE:-${BOB_DIR}/settings}"

if [ -r "$BOB_SETTINGS_FILE" ]; then
    source "$BOB_SETTINGS_FILE"
fi

# General

BOB_EDIT="${BOB_EDIT}"
BOB_DEBUG="${BOB_DEBUG}"
BOB_DRY_RUN="${BOB_DRY_RUN}"

# Git repositories

BOB_DIST_GIT="${BOB_DIST_GIT:-${BOB_SAVED_DIST_GIT:-.}}"
BOB_DIST_GIT_FORK="${BOB_DIST_GIT_FORK:-${BOB_SAVED_DIST_GIT_FORK:-fork}}"
BOB_DIST_GIT_UPSTREAM="${BOB_DIST_GIT_UPSTREAM:-${BOB_SAVED_DIST_GIT_UPSTREAM:-origin}}"
BOB_SRC_GIT="${BOB_SRC_GIT:-${BOB_SAVED_SRC_GIT}}"
BOB_SRC_GIT_FORK="${BOB_SRC_GIT_FORK:-${BOB_SAVED_SRC_GIT_FORK:-fork}}"
BOB_SRC_GIT_UPSTREAM="${BOB_SRC_GIT_UPSTREAM:-${BOB_SAVED_SRC_GIT_UPSTREAM:-origin}}"


[ -n "$BOB_DIST_GIT" ] || error 2 "Dist git path not set"
[ -n "$BOB_SRC_GIT" ] || error 2 "Source git path not set"


BOB_DIST_GIT_MAIN="${BOB_DIST_GIT_MAIN:-${BOB_SAVED_DIST_GIT_MAIN:-`dist_git_q rev-parse --abbrev-ref HEAD`}}"
BOB_SRC_GIT_MAIN="${BOB_SRC_GIT_MAIN:-${BOB_SAVED_SRC_GIT_MAIN:-$BOB_DIST_GIT_MAIN}}"

BOB_RELEASE="${BOB_RELEASE:-${BOB_SAVED_RELEASE:-$BOB_DIST_GIT_MAIN}}"

BOB_PACKAGE="${BOB_PACKAGE:-${BOB_PACKAGE_SAVED:-`find "$BOB_DIST_GIT"-name '*.spec' -printf '%f' | head -n 1 | sed -n -e 's/.spec$//p'`}}"

case "$BOB_RELEASE" in
    c*s)
        BOB_XPKG="${BOB_XPKG:-${BOB_SAVED_XPKG:-centpkg}}"
        BOB_FLAVOR="${BOB_FLAVOR:-${BOB_SAVED_FLAVOR:-centos}}"
        echo "Centos Stream ($BOB_RELEASE) build using $BOB_XPKG"
        ;;
    rhel-*)
        BOB_XPKG="${BOB_XPKG:-rhpkg}"
        BOB_FLAVOR="${BOB_FLAVOR:-rhel}"
        echo "RHEL Z-Stream ($BOB_RELEASE) build using $BOB_XPKG"
        ;;
    f*|rawhide)
        BOB_XPKG="${BOB_XPKG:-fedpkg}"
        BOB_FLAVOR="${BOB_FLAVOR:-fedora}"
        echo "Fedora ($BOB_RELEASE) build using $BOB_XPKG"
        ;;
    *)
        error 2 "Unknown release $BOB_RELEASE"
        ;;
esac

#
# Abort work
#

if [ "$1" = "abort" ]; then

    while [ -s "$BOB_JOURNAL_FILE" ]; do
        read -r TYPE ARGS < <(peek_last_line "$BOB_JOURNAL_FILE")

        if [[ "$TYPE" =~ ^-.* ]]; then
            DRY_RUN_SAVED=yes
            DRY_RUN_SAVED_VALUE="${BOB_DRY_RUN}"
            BOB_DRY_RUN=yes
            TYPE="${TYPE:1}"
        fi

        case "$TYPE" in
            gather) # -
                ;;
            tag) # VERSION -
                src_git tag -d "v$ARGS"
                ;;
            pushtag) # VERSION -
                echo -n "Please delete tag v$ARGS from upstream repository and then press Return to confirm..."
                read -r NOP
                ;;
            branch) # BRANCHNAME -
                dist_git switch "$BOB_DIST_GIT_MAIN"
                dist_git branch -D "$ARGS"
                ;;
            pick) # ISSUE COMMIT -
                dist_git reset --hard HEAD^
                ;;
            update) # VERSION -
                dist_git clean -f "$BOB_PACKAGE"'*.tar.gz' "$BOB_PACKAGE"'*.rpm'
                dist_git restore "$BOB_PACKAGE.spec" "sources"
                ;;
            commit) # -
                dist_git reset --hard HEAD^
                ;;
            push) # BRANCHNAME
                echo "Please delete branch $ARGS from the fork repository and then press Return to confirm..."
                read -r NOP
                ;;
            scratch) # -
                run rm -f "$BOB_PACKAGE*.rpm"
                ;;
            wait_zuul) # -
                ;;
            wait_sync) # -
                ;;
            build) # -
                echo "Aborting final build cannot be done here"
                ;;
            ""|\#*)
                ;;
            *)
                error 1 "Unknown journal entry $TYPE"
                ;;
        esac

        if [ -n "$DRY_RUN_SAVED" ]; then
            BOB_DRY_RUN="$DRY_RUN_SAVED_VALUE"
            DRY_RUN_SAVED=""
            DRY_RUN_SAVED_VALUE=""
        fi

        shift_last_line "$BOB_JOURNAL_FILE" > /dev/null
    done

    rm -rf "$BOB_DIR"
    exit
fi

#
# Prepare work
#

if [ ! -r "$BOB_TODO_FILE" ]; then
    sync_main

    mkdir -p "$BOB_DIR"

    IFS='.' read -r PREV_MAJOR PREV_MINOR PREV_PATCH \
        < <(dist_git_q show "$BOB_DIST_GIT_MAIN:$BOB_PACKAGE.spec" | sed -n -e 's/^Version: //p')
    case "$BOB_FLAVOR" in
        centos)
            VERSION="$PREV_MAJOR.$PREV_MINOR.$(( $PREV_PATCH + 1 ))"
            ;;
        rhel)
            VERSION="-r"
            ;;
        fedora)
            VERSION="$PREV_MAJOR.$(( $PREV_MINOR + 1 ))"
            ;;
        *)
            echo "Cannot determine version automatically, please set"
            ;;
    esac
    while true; do
        echo -n "Version [$VERSION]: "
        read -r USER_VERSION
        if [ -n "$USER_VERSION" ]; then
            VERSION="$USER_VERSION"
        fi
        [ -z "$VERSION" ] || break
    done

    echo "gather" >> "$BOB_TODO_FILE.tmp"
    if [ "$VERSION" != "-r" ]; then
        echo "tag $VERSION" >> "$BOB_TODO_FILE.tmp"
        echo "pushtag $VERSION" >> "$BOB_TODO_FILE.tmp"
    fi

    BRANCH="build-`date +%Y%m%d`"
    echo "branch $BRANCH" >> "$BOB_TODO_FILE.tmp"

    echo "Please provide commits for backporting to dist-git in format RHEL-XXXX COMMITS... (use blank line or EOF to end input):"
    while read -r ISSUE COMMITS; do
        if [ -z "$ISSUE" ]; then
            break
        fi
        for COMMIT in $COMMITS; do
            echo "pick $ISSUE $COMMIT" >> "$BOB_TODO_FILE.tmp"
        done
    done

    echo "update $VERSION" >> "$BOB_TODO_FILE.tmp"
    echo "commit" >> "$BOB_TODO_FILE.tmp"
    echo "push $BRANCH" >> "$BOB_TODO_FILE.tmp"

    echo "scratch" >> "$BOB_TODO_FILE.tmp"
    echo "wait_zuul" >> "$BOB_TODO_FILE.tmp"

    echo "wait_sync" >> "$BOB_TODO_FILE.tmp"
    echo "build" >> "$BOB_TODO_FILE.tmp"

    echo "# Lines starting with # are interpreted as comments" >> "$BOB_TODO_FILE.tmp"
    echo "# Prepend line with '-' (without space) to dry-run a single task" >> "$BOB_TODO_FILE.tmp"
    echo "# To pause between tasks, insert a new line containing 'pause' between them"

    mv "$BOB_TODO_FILE.tmp" "$BOB_TODO_FILE"
    set | sed -n -e 's/^BOB_/BOB_SAVED_/p' > "$BOB_SETTINGS_FILE"

else
    if [ "$1" = "continue" ]; then
        echo "Continuing..."
    else
        error 1 "Build is in progress, use '$0 continue' to continue"
    fi
fi

if [ -n "$BOB_EDIT" ]; then
    if [ -n "$VISUAL" ]; then
        "$VISUAL" "$BOB_TODO_FILE"
    elif [ -n "$EDITOR" ]; then
        "$EDITOR" "$BOB_TODO_FILE"
    else
        error 1 "No editor configured in VISUAL or EDITOR environment variables"
    fi
fi

#
# Do work
#

while [ -s "$BOB_TODO_FILE" ]; do
    read -r TYPE ARGS < <(peek_line "$BOB_TODO_FILE")

    if [[ "$TYPE" =~ ^-.* ]]; then
        DRY_RUN_SAVED=yes
        DRY_RUN_SAVED_VALUE="${BOB_DRY_RUN}"
        BOB_DRY_RUN=yes
        TYPE="${TYPE:1}"
    fi

    case "$TYPE" in
        gather) # -
            FROM=`dist_git_q show "$BOB_DIST_GIT_MAIN:$BOB_PACKAGE.spec" | sed -n -e 's/%global commit //p'`
            src_git_q fetch --all
            if [ "$BOB_FLAVOR" = "fedora" ]; then
                src_git_q log "$FROM..$BOB_SRC_GIT_MAIN" \
                        --format="- %s" \
                    | sed '/^\s*$/d; s/ $//' \
                    | sed 's|https://issues.redhat.com/browse/||' > "$BOB_COMMITS_FILE"
            else
                src_git_q log "$FROM..$BOB_SRC_GIT_MAIN" \
                        --format="- %s %n%(trailers:key=Resolves)%(trailers:key=Related)%(trailers:key=Fix)" \
                    | sed '/^\s*$/d; s/ $//' \
                    | sed 's|https://issues.redhat.com/browse/||' > "$BOB_COMMITS_FILE"
            fi
            ;;
        tag) # VERSION -
            src_git tag "v$ARGS" "$BOB_SRC_GIT_MAIN"
            ;;
        pushtag) # VERSION -
            src_git push "$BOB_SRC_GIT_FORK" "v$ARGS"
            src_git push "$BOB_SRC_GIT_UPSTREAM" "v$ARGS"
            ;;
        branch) # BRANCHNAME -
            dist_git switch -c "$ARGS"
            ;;
        pick) # ISSUE COMMIT -
            read -r ISSUE COMMIT < <(echo "$ARGS")
            {
                if src_git_q rev-parse --verify --quiet CHERRY_PICK_HEAD; then
                    src_git cherry-pick --continue
                else
                    src_git cherry-pick "$COMMIT"
                fi
            } || error 1 "Please resolve conflicts (without commiting) and run '$0 continue'"

            {
                MESSAGE="`src_git_q show -s --format="%B" "$COMMIT"`"
                if ! echo "$MESSAGE" | grep -q "^Resolves: $ISSUE"; then
                    COUNT="`echo "$MESSAGE" | grep -c "^Resolves:" || true`"
                    if [ "$COUNT" -eq 0 ]; then
                        echo -en "$MESSAGE\n\nResolves: $ISSUE" | src_git commit --amend --file -
                    elif [ "$COUNT" -eq 1 ]; then
                        echo "$MESSAGE" | sed 's/^Resolves:.*$/Resolves: '"$ISSUE"'/' | src_git commit --amend --file -
                    else
                        echo -n "Cannot automatically update Resolve line. Please edit the commit message manually after pressing Return..."
                        read -r NOP
                        src_git commit --amend
                    fi
                fi
            } || {
                src_git reset --hard HEAD^
                error 1 "Failed to manually update. Try again by running '$0 continue'"
            }
            ;;
        update) # VERSION -
            dist_git clean -f "$BOB_PACKAGE"'*.tar.gz' "$BOB_PACKAGE"'*.rpm'
            dist_git restore "$BOB_PACKAGE.spec" "sources"
            run env -C "$BOB_DIST_GIT" "./make-rhat-patches.sh"
            if [ "$ARGS" = "-r" ]; then
                run rpmdev-bumpspec -r -c "`cat "$BOB_COMMITS_FILE"`" "$BOB_PACKAGE.spec"
            else
                run rpmdev-bumpspec -n "$ARGS" -c "`cat "$BOB_COMMITS_FILE"`" "$BOB_PACKAGE.spec"
            fi
            sed -n '1,/^%changelog/d;1,/^$/p' "$BOB_PACKAGE.spec" > "$BOB_CHANGELOG_FILE"
            run "$BOB_XPKG" new-sources `cat sources | sed 's/^.*(\(.*\)).*$/\1/' | xargs`
            ;;
        commit) # -
            dist_git commit "$BOB_PACKAGE.spec" "sources" --file "$BOB_CHANGELOG_FILE"
            ;;
        push) # BRANCHNAME
            dist_git push "$BOB_DIST_GIT_FORK" "$ARGS"
            ;;
        scratch) # -
            run "$BOB_XPKG" build --scratch --srpm --release "$BOB_RELEASE"
            ;;
        wait_zuul) # -
            echo -n "Please wait for ZUUL to approve MR, merge it and then press Return to confirm..."
            read -r NOP
            ;;
        wait_sync) # -
            echo "Waiting for changes to propagate"
            WAIT_TIME=$(( 60 * 5 ))
            while [ "$WAIT_TIME" -gt 0 ]; do
                printf "\r%02d:%02d" $(( "$WAIT_TIME" / 60 )) $(( "$WAIT_TIME" % 60 ))
                sleep 1
                WAIT_TIME=$(( "$WAIT_TIME" - 1 ))
            done
            printf "\rChanges should be propagated by now\n"
            sync_main
            ;;
        build) # -
            run "$BOB_XPKG" build --release "$BOB_RELEASE"
            ;;
        pause)
            shift_line "$BOB_TODO_FILE" >> /dev/null
            echo "Run '$0 continue' to continue..."
            exit 0
            ;;
        ""|\#*)
            ;;
        *)
            error 1 "Unknown todo $TYPE"
            ;;
    esac

    if [ -n "$DRY_RUN_SAVED" ]; then
        BOB_DRY_RUN="$DRY_RUN_SAVED_VALUE"
        DRY_RUN_SAVED=""
        DRY_RUN_SAVED_VALUE=""
    fi

    shift_line "$BOB_TODO_FILE" >> "$BOB_JOURNAL_FILE"
done

rm -rf "$BOB_DIR"
