# Bob

Bob will build everything¹.

¹ if it is compatible with the [selinux-policy build process][1]

[1]: https://github.com/fedora-selinux/selinux-policy/blob/c9s/README-BUILD.md


## Backporting commits

Use the [`./backport.sh`](./backport.sh) script to backport multiple commits
related to an issue in separate branches depending on the issue.

```sh
./backport.sh [continue|abort]
```

On first start, the script expects input in the following format:

```
ISSUE_NUMBER COMMITREF_1 [COMMITREF_N...]
```

For example:

```
RHEL-36291 eeff84bb
RHEL-26663 4adaa46d eb644c87
```

This would create following branches with commits in the repository:

```
build-`date`-RHEL-36291     (parent $BOB_GIT_MAIN)              cherry-pick eeff84bb
build-`date`-RHEL-26663     (parent build-`date`-RHEL-36291)    cherry-pick 4adaa46d eb644c87
```

All created branches are pushed to the `$BOB_GIT_FORK` remote and can be merged
one by one to the upstream repository with all merges as fast-forward.

When the script is interrupted at any moment (due to an error, or `pause`
task), the script can be run with `continue` to resume work or `abort` to
revert to the previous state.

### Configuration

```sh
BOB_DIR=".bob_backport"
```
Temporary directory to save work files, will be deleted when the work is
finished or aborted
```sh
BOB_TODO_FILE="${BOB_DIR}/todo"
```
File storing tasks that will be executed
```sh
BOB_JOURNAL_FILE="${BOB_DIR}/journal"
```
File storing done tasks (used for abort)
```sh
BOB_SETTINGS_FILE="${BOB_DIR}/settings"
```
File storing values of certain environment variables for subsequent calls to
the script

```sh
BOB_EDIT=""
```
Open an editor with the `$BOB_TODO_FILE` file before it is executed if true
```sh
BOB_DRY_RUN=""
```
Turn off execution of commands that would change the repository (just print
them) if true

```sh
BOB_GIT="."
```
Path of the git work tree where commits should be backported to (saved)
```sh
BOB_GIT_FORK="fork"
```
Forked repository remote name (saved)
```sh
BOB_GIT_UPSTREAM="origin"
```
Upstream repository remote name (saved)
```sh
BOB_GIT_MAIN="`git -C "$BOB_GIT" rev-parse --abbrev-ref HEAD`"
```
Main branch in the repository (will be synced with upstream on start and
switched to on finish) (saved)

Environment variables marked as _(saved)_ are saved to the `$BOB_SETTINGS_FILE`
file. The values saved are used as default values in subsequent calls to the
script with `continue` or `abort` (i.e. the saved value is used if the value is
not provided explicitly).


## Creating a new build

Use the [`./build.sh`](./build.sh) to create a new Fedora/CentOS Stream/RHEL
Z-Stream build.

```sh
./backport.sh [continue|abort]
```

On the first start, the script expects new version number (or blank line to
compute it automatically) and issue numbers with commits that should be
backported to the dist git repository in the same form as `./backport.sh`. The
difference is, branches are not created for each issue, everything is done in
the `build-\`date\`` branch created by the script in the beginning.

The version can be any string that will be passed to `rpmdev-bumpspec -n` or
`-r` to call `rpmdev-bumpspec -r` instead.

Automatic version calculation depends on the build flavour. Previous version is
parsed from the spec file.

| Build flavour | Default new version                   |
| ------------- | ------------------------------------- |
| Fedora        | `$MAJOR . $(( $MINOR + 1 ))`          |
| CentOS Stream | `$MAJOR . $MINOR . $(( $PATCH + 1 ))` |
| RHEL Z-Stream | `-r`                                  |

If the build flavour is not specified explicitly, it is derived from the name
of `$BOB_DIST_GIT_MAIN` branch.

| Branch pattern | Default build flavour | Default packaging tool |
| -------------- | --------------------- | ---------------------- |
| `f* | rawhide` | Fedora                | `fedpkg`               |
| `c*s`          | CentOS Stream         | `centpkg`              |
| `rhel-*`       | RHEL Z-Stream         | `rhpkg`                |

Base for the changelog commit messages is also parsed from the spec file.

When the script is interrupted at any moment (due to an error, or `pause`
task), the script can be run with `continue` to resume work or `abort` to
revert to the previous state.

### Configuration

```sh
BOB_DIR="${BOB_DIR:-.bob_build}"
```
Temporary directory to save work files, will be deleted when the work is
finished or aborted
```sh
BOB_TODO_FILE="${BOB_DIR}/todo"
```
File storing tasks that will be executed
```sh
BOB_JOURNAL_FILE="${BOB_DIR}/journal"
```
File storing done tasks (used for abort)
```sh
BOB_COMMITS_FILE="${BOB_DIR}/commits"
```
File storing commit messages for changelog
```sh
BOB_CHANGELOG_FILE="${BOB_DIR}/changelog"
```
File storing changelog entry for commit message
```sh
BOB_SETTINGS_FILE="${BOB_DIR}/settings"
```
File storing values of certain environment variables for subsequent calls to
the script

```sh
BOB_EDIT=""
```
Open an editor with the `$BOB_TODO_FILE` file before it is executed if true
```sh
BOB_DRY_RUN=""
```
Turn off execution of commands that would change the repository (just print
them) if true

```sh
BOB_DIST_GIT="."
```
Path of the dist git work tree (saved)
```sh
BOB_DIST_GIT_FORK="fork"
```
Name of the forked dist git remote (saved)
```sh
BOB_DIST_GIT_UPSTREAM="origin"
```
Name of the upstream dist git remote (saved)

```sh
BOB_DIST_GIT_MAIN="`dist_git_q rev-parse --abbrev-ref HEAD`"
```
Name of the dist git main branch (will be synced in the beginning) (saved)

```sh
BOB_SRC_GIT=""
```
Path of the source git work tree (saved)
```sh
BOB_SRC_GIT_FORK="fork"
```
Name of the forked source git remote (tag will be pushed to this remote) (saved)
```sh
BOB_SRC_GIT_UPSTREAM="origin"
```
Name of the upstream source git remote (tag will be pushed to this remote) (saved)
```sh
BOB_SRC_GIT_MAIN="$BOB_DIST_GIT_MAIN"
```
Name of the source git main branch (saved)

```sh
BOB_RELEASE="$BOB_DIST_GIT_MAIN"
```
Release to build (saved)
```sh
BOB_PACKAGE="`find "$BOB_DIST_GIT"-name '*.spec' -printf '%f' | head -n 1 | sed -n -e 's/.spec$//p'`"
```
Name of the package (saved)
```sh
BOB_FLAVOR="fedora|centos|rhel" # default depends on the main branch name
```
Flavour of the build (saved)
```sh
BOB_XPKG="fedpkg|centpkg|rhpkg" # default depends on the flavour
```
Packaging tool command to use (saved)

Environment variables marked as _(saved)_ are saved to the `$BOB_SETTINGS_FILE`
file. The values saved are used as default values in subsequent calls to the
script with `continue` or `abort` (i.e. the saved value is used if the value is
not provided explicitly).
