#!/bin/bash

set -e
set -o pipefail

#
# Utility functions
#

function warn {
    echo "$@" >&2
}

function error {
    CODE="$1"
    shift
    warn "$@"
    exit "$CODE"
}

function run {
    if [ -n "$BOB_DRY_RUN" ]; then
        echo "= $@"
    else
        echo "+ $@"
    fi
    if [ -z "$BOB_DRY_RUN" ]; then
        "$@"
    fi
}

function peek_line {
    head -n1 "$1"
}

function shift_line {
    peek_line "$1"
    sed -i '1d' "$1"
}

function peek_last_line {
    tail -n1 "$1"
}

function shift_last_line {
    peek_last_line "$1"
    sed -i '$d' "$1"
}

#
# Git utility functions
#

function run_git_q {
    git -C "$BOB_GIT" "$@"
}

function run_git {
    run git -C "$BOB_GIT" "$@"
}

function switch_main {
    run_git switch "$BOB_GIT_MAIN"
}

function sync_main {
    switch_main
    run_git pull "$BOB_GIT_UPSTREAM"
}

#
# Settings
#

# Files

BOB_DIR="${BOB_DIR:-.bob_backport}"
BOB_TODO_FILE="${BOB_TODO_FILE:-${BOB_DIR}/todo}"
BOB_JOURNAL_FILE="${BOB_JOURNAL_FILE:-${BOB_DIR}/journal}"
BOB_SETTINGS_FILE="${BOB_SETTINGS_FILE:-${BOB_DIR}/settings}"

if [ -r "$BOB_SETTINGS_FILE" ]; then
    source "$BOB_SETTINGS_FILE"
fi

# General

BOB_EDIT="${BOB_EDIT}"
BOB_DEBUG="${BOB_DEBUG}"
BOB_DRY_RUN="${BOB_DRY_RUN}"

# Git repositories

BOB_GIT="${BOB_GIT:-${BOB_SAVED_GIT}}"
BOB_GIT_FORK="${BOB_GIT_FORK:-${BOB_SAVED_GIT_FORK:-fork}}"
BOB_GIT_UPSTREAM="${BOB_GIT_UPSTREAM:-${BOB_SAVED_GIT_UPSTREAM:-origin}}"
BOB_GIT_MAIN="${BOB_GIT_MAIN:-${BOB_SAVED_GIT_MAIN:-`run_git_q rev-parse --abbrev-ref HEAD`}}"

#
# Abort work
#

if [ "$1" = "abort" ]; then

    if run_git_q rev-parse --verify --quiet CHERRY_PICK_HEAD; then
        run_git cherry-pick --abort
    fi
    
    switch_main
    
    while [ -s "$BOB_JOURNAL_FILE" ]; do
        read -r TYPE ARGS < <(peek_last_line "$BOB_JOURNAL_FILE")
    
        case "$TYPE" in
            branch)
                run_git branch -D "$ARGS"
                ;;
            push)
                echo -n "Please delete branch $ARGS in the fork and origin repositories and then press Return to confirm..."
                read -r NOP
                ;;
            pick)
                ;;
            pause)
                ;;
            ""|\#*)
                ;;
            *)
                warn "Unknown journal line $TYPE $ARGS"
                ;;
        esac
        
        shift_last_line "$BOB_JOURNAL_FILE" >/dev/null
    done
    
    rm -rf "$BOB_DIR"
    exit
fi

#
# Prepare work
#

if [ ! -r "$BOB_TODO_FILE" ]; then
    sync_main

    mkdir -p "$BOB_DIR"

    echo "Please provide commits for backporting in format RHEL-XXXX COMMITS... (use blank line or EOF to end input):"

    while read -r ISSUE COMMITS; do
        if [ -z "$ISSUE" ]; then
            break
        fi
        BRANCH="build-`date +%Y%m%d`-$ISSUE"
        echo "branch $BRANCH" >> "$BOB_TODO_FILE.tmp"
        for COMMIT in $COMMITS; do
            echo "pick $ISSUE $COMMIT" >> "$BOB_TODO_FILE.tmp"
        done
        BRANCHES="${BRANCHES}push $BRANCH\n"
    done

    echo -en "$BRANCHES" >> "$BOB_TODO_FILE.tmp"

    echo "# Lines starting with # are interpreted as comments" >> "$BOB_TODO_FILE.tmp"
    echo "# To pause between tasks, insert a new line containing 'pause' between them"

    mv "$BOB_TODO_FILE.tmp" "$BOB_TODO_FILE"
    set | grep "^BOB_" > "$BOB_SETTINGS_FILE"
elif [ "$1" = "continue" ]; then
    echo "Continuing with backporting..."
else
    error 1 "Backport is in progress, abort or continue"
fi

if [ -n "$BOB_EDIT" ]; then
    if [ -n "$VISUAL" ]; then
        "$VISUAL" "$BOB_TODO_FILE"
    elif [ -n "$EDITOR" ]; then
        "$EDITOR" "$BOB_TODO_FILE"
    else
        error 1 "No editor configured in VISUAL or EDITOR environment variables"
    fi
fi

#
# Do work
#

while [ -s "$BOB_TODO_FILE" ]; do
    read -r TYPE ARGS < <(peek_line "$BOB_TODO_FILE")

    case "$TYPE" in
        branch)
            run_git switch -c "$ARGS"
            ;;
        pick)
            read -r ISSUE COMMIT < <(echo "$ARGS")
            {
                if run_git_q rev-parse --verify --quiet CHERRY_PICK_HEAD; then
                    run_git cherry-pick --continue
                else
                    run_git cherry-pick "$COMMIT"
                fi
            } || error 1 "Please resolve conflicts (without commiting) and run '$0 continue' to continue"

            {
                MESSAGE="`run_git_q show -s --format="%B" "$COMMIT"`"
                if ! echo "$MESSAGE" | grep -q "^Resolves: $ISSUE"; then
                    COUNT="`echo "$MESSAGE" | grep -c "^Resolves:" || true`"
                    if [ "$COUNT" -eq 0 ]; then
                        echo -en "$MESSAGE\n\nResolves: $ISSUE" | run_git commit --amend --file -
                    elif [ "$COUNT" -eq 1 ]; then
                        echo "$MESSAGE" | sed 's/^Resolves:.*$/Resolves: '"$ISSUE"'/' | run_git commit --amend --file -
                    else
                        echo -n "Cannot automatically update Resolve line. Please edit the commit message manually after pressing Return..."
                        read -r NOP
                        run_git commit --amend
                    fi
                fi
            } || {
                run_git reset --hard HEAD^
                error 1 "Failed to manually update. Try again by running '$0 continue'"
            }
            ;;
        push)
            run_git switch "$ARGS"
            run_git push "$BOB_GIT_FORK" "$ARGS"
            echo -n "Please merge branch $ARGS to the main branch and confirm..."
            read -r NOP
            ;;
        pause)
            echo "Paused, run '$0 continue' to continue when ready"
            shift_line "$BOB_TODO_FILE" >> "$BOB_JOURNAL_FILE"
            exit 0
            ;;
        ""|\#*)
            ;;
        *)
            error 1 "Unknown work line $TYPE $ARGS"
            ;;
    esac
    
    shift_line "$BOB_TODO_FILE" >> "$BOB_JOURNAL_FILE"
done

sync_main

rm -rf "$BOB_DIR"
